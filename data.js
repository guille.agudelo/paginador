module.exports = dependencies = {
  "data": [
        {
            "id": "1",
            "name": "Dirección nacional"
        },
        {
            "id": "2",
            "name": "Dirección territorial"
        },
        {
            "id": "3",
            "name": "Subdirección académica"
        },
        {
            "id": "4",
            "name": "Dirección nacional"
        },
        {
            "id": "5",
            "name": "Dirección territorial"
        },
        {
            "id": "6",
            "name": "Subdirección académica"
        },
        {
            "id": "7",
            "name": "Dirección nacional"
        },
        {
            "id": "8",
            "name": "Dirección territorial"
        },
        {
            "id": "9",
            "name": "Subdirección académica"
        },
        {
            "id": "10",
            "name": "Dirección nacional"
        },
        {
            "id": "11",
            "name": "Dirección territorial"
        },
        {
            "id": "12",
            "name": "Subdirección académica"
        },
        {
            "id": "13",
            "name": "Dirección nacional"
        },
        {
            "id": "14",
            "name": "Dirección territorial"
        },
        {
            "id": "15",
            "name": "Subdirección académica"
        },
        {
            "id": "16",
            "name": "Dirección nacional"
        },
        {
            "id": "17",
            "name": "Dirección territorial"
        },
        {
            "id": "18",
            "name": "Subdirección académica"
        },
        {
            "id": "19",
            "name": "Dirección territorial"
        },
        {
            "id": "20",
            "name": "Subdirección académica"
        }
    ]
}

module.exports = offices = {
  "data": [
        {
            "id": "1",
            "name": "Oficina 1"
        },
        {
            "id": "2",
            "name": "Edificio 2"
        },
        {
            "id": "3",
            "name": "Ofimarket 3"
        },
        {
            "id": "4",
            "name": "Edificio 2"
        },
        {
            "id": "5",
            "name": "Ofimarket 3"
        },
        {
            "id": "6",
            "name": "Oficina 1"
        },
        {
            "id": "7",
            "name": "Edificio 2"
        },
        {
            "id": "8",
            "name": "Ofimarket 3"
        },
        {
            "id": "9",
            "name": "Edificio 2"
        },
        {
            "id": "10",
            "name": "Ofimarket 3"
        },
        {
            "id": "11",
            "name": "Oficina 1"
        },
        {
            "id": "12",
            "name": "Edificio 2"
        },
        {
            "id": "13",
            "name": "Ofimarket 3"
        },
        {
            "id": "14",
            "name": "Edificio 2"
        },
        {
            "id": "15",
            "name": "Ofimarket 3"
        },
        {
            "id": "16",
            "name": "Oficina 1"
        },
        {
            "id": "17",
            "name": "Edificio 2"
        },
        {
            "id": "18",
            "name": "Ofimarket 3"
        },
        {
            "id": "19",
            "name": "Edificio 2"
        },
        {
            "id": "20",
            "name": "Ofimarket 3"
        }
    ]

}

module.exports = faculties = {
  "data": [
        {
            "id": "1",
            "name": "Pregrado"
        },
        {
            "id": "2",
            "name": "Postgrado"
        },
        {
            "id": "3",
            "name": "Investigación"
        },
        {
            "id": "4",
            "name": "Postgrado"
        },
        {
            "id": "5",
            "name": "Investigación"
        },
        {
            "id": "6",
            "name": "Pregrado"
        },
        {
            "id": "7",
            "name": "Postgrado"
        },
        {
            "id": "8",
            "name": "Investigación"
        },
        {
            "id": "9",
            "name": "Postgrado"
        },
        {
            "id": "10",
            "name": "Investigación"
        },
        {
            "id": "11",
            "name": "Pregrado"
        },
        {
            "id": "12",
            "name": "Postgrado"
        },
        {
            "id": "13",
            "name": "Investigación"
        },
        {
            "id": "14",
            "name": "Postgrado"
        },
        {
            "id": "15",
            "name": "Investigación"
        },
        {
            "id": "16",
            "name": "Pregrado"
        },
        {
            "id": "17",
            "name": "Postgrado"
        },
        {
            "id": "18",
            "name": "Investigación"
        },
        {
            "id": "19",
            "name": "Postgrado"
        },
        {
            "id": "20",
            "name": "Investigación"
        },
    ]

}
