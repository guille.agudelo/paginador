(function () {

function getParams (params) {
  if (params === null || params === undefined) return { page: 1, sort_by: 'id:asc' }

  const pars = params.split('&')
  const objParams = {}

  pars.forEach(item => {
    const param = item.split('=')
    objParams[param[0]] = param[1]
  })

  if (!objParams.sort_by) objParams.sort_by = 'id:asc'

  return objParams
}

function getPageUrls (path, params, currentPage, lastPage) {
  const objPageUrls = {}

  objPageUrls.firstPageUrl = path + `?page=1&sort_by=${params.sort_by}`
  objPageUrls.lastPageUrl = path + `?page=${lastPage}&sort_by=${params.sort_by}`
  objPageUrls.nextPageUrl = currentPage === lastPage
                            ? null
                            : path + `?page=${currentPage + 1}&sort_by=${params.sort_by}`
  objPageUrls.prevPageUrl = currentPage === 1
                            ? null
                            : path + `?page=${currentPage - 1}&sort_by=${params.sort_by}`

  return objPageUrls
}

function sortRecords (records, sortBy) {
  const sortField = sortBy.split(':')[0]
  const sortOrder = sortBy.split(':')[1]

  const sortedRecords = records.sort((a, b) => {
    // Converts strings to numbers to correctly sort by numbers
    if (!isNaN(a[sortField])) {
      a[sortField] = Number(a[sortField])
      b[sortField] = Number(b[sortField])
    }

    if (a[sortField] > b[sortField]) {
      return  sortOrder === 'asc' ? 1 : -1;
    }
    if (a[sortField] < b[sortField]) {
      return  sortOrder === 'asc' ? -1 : 1;
    }

    return 0;
  })

  return sortedRecords
}

/**
 * Pagina y ordena un array segun parametros recibidos.
 *
 * This function recieves an array of records to be paginated, a integer number indicating the numbers of records
 * to send per page and the full request URL containing two params: the current page to retrieve and the criteria
 * for sorting.
 * The two params are:
 * -> 'page'. Indicates the page to retrieve
 * -> 'sort_by'. Indicates the field for which the records will be sorted and the sort order (asc or desc)
 * Example of the two URL params: ?page=2&sort_by=name:asc.
 *
 * @param {array}  records       Array with records. p.ej. [ {'id': 1, 'name': 'aa'}, {'id': 2, 'name': 'bb'} ].
 * @param {int}    perPage       Number of records in one page.
 * @param {string} fullUrl       Full request URL. p.ej. 'https://domain.com/api/users?page=2&sort_by=name:asc'.
 *
 * @return {Object} Object with pagination meta data and current page records.
 */
module.exports = function (records, perPage, fullUrl) {
  perPage = parseInt(perPage)
  const path = fullUrl.split('?')[0]
  const params = getParams(fullUrl.split('?')[1])

  const currentPage = params.page ? parseInt(params.page) : 1
  const total = records.length
  const lastPage = Math.ceil(total / perPage)
  const from = (currentPage - 1) * perPage + 1
  const to = from + perPage - 1 > total ? total : from + perPage - 1

  const pageUrls = getPageUrls(path, params, currentPage, lastPage)

  const sortedRecords = sortRecords(records, params.sort_by)

  const recordsToSend = sortedRecords.slice(from - 1, to)

  return {
    "total": total,
    "per_page": perPage,
    "current_page": currentPage,
    "last_page": lastPage,
    "first_page_url": pageUrls.firstPageUrl,
    "last_page_url": pageUrls.lastPageUrl,
    "next_page_url": pageUrls.nextPageUrl,
    "prev_page_url": pageUrls.prevPageUrl,
    "path": path,
    "from": from,
    "to": to,
    "data": recordsToSend
  }
}

}());
