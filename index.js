const express = require('express')
var cors = require('cors')
const app = express()
const port = 3000

const paginate = require('./paginate')
require('./data')

app.use(cors())

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/dependencies', (req, res) => {
  const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl
  res.json(paginate(dependencies.data, 5, fullUrl))
})

app.get('/dependencies/:id', (req, res) => {
  const record = dependencies.data.find(item => parseInt(item.id) === parseInt(req.params.id))
  res.json({record})
})

app.get('/offices', (req, res) => {
  const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl
  res.json(paginate(offices.data, 10, fullUrl))
})

app.get('/offices/:id', (req, res) => {
  const record = offices.data.find(item => parseInt(item.id) === parseInt(req.params.id))
  res.json({record})
})

app.get('/dependencies/:id', (req, res) => {
  const record = dependencies.data.find(item => parseInt(item.id) === parseInt(req.params.id))
  res.json({record})
})

app.get('/faculties', (req, res) => {
  const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl
  res.json(paginate(faculties.data, 15, fullUrl))
})

app.get('/faculties/:id', (req, res) => {
  const record = faculties.data.find(item => parseInt(item.id) === parseInt(req.params.id))
  res.json({record})
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))
